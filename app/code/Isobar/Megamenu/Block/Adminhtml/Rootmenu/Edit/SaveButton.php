<?php
namespace Isobar\Megamenu\Block\Adminhtml\Rootmenu\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 * @package Magento\Customer\Block\Adminhtml\Edit
 */
class SaveButton extends GenericButton implements ButtonProviderInterface
{
    public function __construct()
    {
    }

    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Save Root Menu'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }
}
