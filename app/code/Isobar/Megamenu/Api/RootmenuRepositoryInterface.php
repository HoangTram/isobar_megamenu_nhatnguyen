<?php

namespace Isobar\Megamenu\Api;


interface RootmenuRepositoryInterface
{
    /**
     * @param \Isobar\Megamenu\Api\Data\RootmenuInterface $rootmenu
     * @return int
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Isobar\Megamenu\Api\Data\RootmenuInterface $rootmenu);

    /**
     * Get info about rootmenu by id
     *
     * @param int $modelId
     * @return \Isobar\Megamenu\Api\Data\RootmenuInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function get($modelId);

    /**
     * Retrieve root megamenu.
     *
     * @param int $id
     * @return \Isobar\Megamenu\Api\Data\RootmenuInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Delete megamenu
     *
     * @param \Isobar\Megamenu\Api\Data\RootmenuInterface $rootMenu
     * @return bool Will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Isobar\Megamenu\Api\Data\RootmenuInterface $rootMenu);

    /**
     * Delete rootmenu by id
     *
     * @param int $id
     * @return bool will returned True if deleted
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($id);

    /**
     * Get rootmenu list
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Isobar\Megamenu\Api\Data\RootmenuSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Get root id by store id.
     *
     * @param int $storeId
     * @return int
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getRootIdByStoreId($storeId);
}