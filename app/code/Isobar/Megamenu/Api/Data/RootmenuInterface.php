<?php

namespace Isobar\Megamenu\Api\Data;


interface RootmenuInterface
{
    /**
     * Constants defined for keys of data array
     */
    const TITLE             = 'title';
    const STORE_ID          = 'store_id';
    const AS_ROOT_MENU      = 'as_root_menu';
    const STATUS            = 'status';
    const SORT              = 'sort';
    const CREATED_AT        = 'created';

    /**
     * Get root megamenu id
     * @return int|null
     */
    public function getId();

    /**
     * Set root megamenu id
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * Get root megamenu title
     * @return string|null
     */

    /**
     * Get root store id
     * @return int|null
     */
    public function getStoreId();

    /**
     * Set root store id
     * @param int $storeId
     * @return $this
     */
    public function setStoreId($storeId);

    /**
     * Get root megamenu title
     * @return string|null
     */

    public function getTitle();

    /**
     * Set root megamenu title
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * Set root megamenu status
     * @param int $status
     * @return $this
     */
    public function setStatus($status);

    /**
     * Get root megamenu status
     * @return int|null
     */
    public function getStatus();

    /**
     * Set root megamenu as_root_menu
     * @param int $status
     * @return $this
     */
    public function setAsRootMenu($asRootMenu);

    /**
     * Get root megamenu as_root_menu
     * @return int|null
     */
    public function getAsRootMenu();

    /**
     * Set root megamenu sort
     * @param int $sort
     * @return $this
     */
    public function setSort($sort);

    /**
     * Get root megamenu sort
     * @return int|null
     */
    public function getSort();

    /**
     * Get megamenu created date
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set megamenu created date
     *
     * @param string $timeStamp
     * @return $this
     */
    public function setCreatedAt($timeStamp);
}