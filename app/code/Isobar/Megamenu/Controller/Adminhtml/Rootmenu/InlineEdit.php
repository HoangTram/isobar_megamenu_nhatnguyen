<?php
namespace Isobar\Megamenu\Controller\Adminhtml\Rootmenu;

use Magento\Backend\App\Action\Context;
use Isobar\Megamenu\Api\RootmenuRepositoryInterface as RootmenuRepository;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Cms\Api\Data\BlockInterface;

class InlineEdit extends \Magento\Backend\App\Action
{
    /** @var RootmenuRepository  */
    protected $rootMenuRepository;

    /** @var JsonFactory  */
    protected $jsonFactory;

    /**
     * @param Context $context
     * @param ItemRepository $itemRepository
     * @param JsonFactory $jsonFactory
     */
    public function __construct(
        Context $context,
        RootmenuRepository $rootMenuRepository,
        JsonFactory $jsonFactory
    ) {
        parent::__construct($context);
        $this->rootMenuRepository = $rootMenuRepository;
        $this->jsonFactory = $jsonFactory;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->jsonFactory->create();
        $error = false;
        $messages = [];

        if ($this->getRequest()->getParam('isAjax')) {
            $postItems = $this->getRequest()->getParam('items', []);
            if (!count($postItems)) {
                $messages[] = __('Please correct the data sent.');
                $error = true;
            } else {
                foreach (array_keys($postItems) as $itemId) {
                    $item = $this->rootMenuRepository->getById($itemId);
                    try {
                        $item->setData(array_merge($item->getData(), $postItems[$itemId]));
                        $this->rootMenuRepository->save($item);
                    } catch (\Exception $e) {
                        $messages[] = $this->getErrorWithBlockId(
                            $item,
                            __($e->getMessage())
                        );
                        $error = true;
                    }
                }
            }
        }

        return $resultJson->setData([
            'messages' => $messages,
            'error' => $error
        ]);
    }

    /**
     * Add banner title to error message
     *
     * @param ItemInterface $item
     * @param string $errorText
     * @return string
     */
    protected function getErrorWithBlockId(ItemInterface $item, $errorText)
    {
        return '[Menu ID: ' . $item->getId() . '] ' . $errorText;
    }
}
